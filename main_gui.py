import PySimpleGUI as sg
from main import do_magic
#sg.theme('DarkAmber')	# Add a touch of color
# All the stuff inside your window.
layout = [  [sg.Text('Codice persona'), sg.InputText(key='Codice') ],
			[sg.Text('Password'), sg.InputText('', key='Password', password_char='*')],
            [sg.Text('URL'), sg.InputText(key='URL')],
            [sg.Text('Destination folder'), sg.InputText(key='Destination'), sg.FolderBrowse()],
            [sg.Text('Filename (optional'), sg.InputText(key='Filename')],
            [sg.Button('Download'), sg.Button('Cancel')] ]

# Create the Window
window = sg.Window('Ms Stream Downloader', layout)
# Event Loop to process "events" and get the "values" of the inputs
while True:
    event, v = window.read()
    if event in (None, 'Cancel'):	# if user closes window or clicks cancel
        break
    ret = do_magic(v["Codice"], v["Password"], v["URL"], v["Destination"], v["Filename"] if v["Filename"] != "" else None)
    if not ret:
    	print("Error")

window.close()