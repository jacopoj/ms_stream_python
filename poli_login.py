from requests import Session
import re
from lxml import html
import json 


def get_polimi_token(username, password):
	session = Session()

	URL = "https://web.microsoftstream.com/browse"

	canary_regex = r'\"(apiCanary)\":\"((\\\"|[^\"])*)\"'
	ft_regex = r'\"(sFT)\":\"((\\\"|[^\"])*)\"' 
	ctx_regex = r'\"(sCtx)\":\"((\\\"|[^\"])*)\"'
	token_regex = r'\"(AccessToken)\":\"((\\\"|[^\"])*)\"'


	response = session.get('https://web.microsoftstream.com/browse')
	tree = html.fromstring(response.text)
	form = tree.xpath('/html/body/form')[0]
	response = session.post(form.action, data=dict(form.fields))

	canary = re.search(canary_regex, response.text).group(2)
	flow_token = re.search(ft_regex, response.text).group(2)
	ctx = re.search(ctx_regex, response.text).group(2)

	headers = {
	    'canary': canary,
	}

	data = {
	"username":"test@polimi.it",
	"isOtherIdpSupported":True,
	"checkPhones":False,
	"isRemoteNGCSupported":True,
	"isCookieBannerShown":False,
	"isFidoSupported":False,
	"originalRequest":ctx,
	"forceotclogin":False,
	"isExternalFederationDisallowed":False,
	"isRemoteConnectSupported":False,
	"federationFlags":0,
	"isSignup":False,
	"flowToken":flow_token,
	"isAccessPassSupported":True}

	response = session.post('https://login.microsoftonline.com/common/GetCredentialType', headers=headers, data=json.dumps(data)).json()

	poli_url = response["Credentials"]["FederationRedirectUrl"]

	response = session.get(poli_url)


	data = {
	  'login': username,
	  'password': password,
	  'evn_conferma': ''
	}

	response = session.post('https://aunicalogin.polimi.it/aunicalogin/aunicalogin/controller/IdentificazioneUnica.do', data=data)

	for i in range(0,3):
		tree = html.fromstring(response.text)
		form = tree.xpath('/html/body/form')[0]
		response = session.post(form.action, data=dict(form.fields))


	canary = re.search(canary_regex, response.text).group(2)
	flow_token = re.search(ft_regex, response.text).group(2)
	ctx = re.search(ctx_regex, response.text).group(2)


	data = {
	  'LoginOptions': '3',
	  'type': '28',
	  'ctx': ctx,
	  'flowToken': flow_token,
	  'canary': canary,
	  'i2': '',
	  'i17': '',
	  'i18': '',
	  'i19': '33881'
	}

	response = session.post('https://login.microsoftonline.com/kmsi', data=data, allow_redirects=False)

	tree = html.fromstring(response.text)
	form = tree.xpath('/html/body/form')[0]

	response = session.post(form.action, data=dict(form.fields))


	token = "Bearer " + re.search(token_regex, response.text).group(2)

	return token
