from requests import Session
from urllib import parse
from base64 import b64decode
import os
import shutil
import copy
from utils import *
import tempfile
#import logging

TEMP = tempfile.gettempdir()
def get_temp_folder(filename):
    return os.path.join(TEMP, filename, "")
def get_path_file(directory, filename):
    return os.path.join(directory, filename)

class MsStream:
    API_URL = "https://euwe-1.api.microsoftstream.com/api/"

    def __init__(self, token):
        self.session = Session()
        self.token = token
        self.headers = {'authorization': token,
                'accept': 'application/json'}
        self.refresh_token(True)

    def refresh_token(self, new=False):
        if new:
            return self.session.get(MsStream.API_URL + 'refreshtoken?api-version=1.3-private', headers=self.headers)
        else:
            return self.session.get(MsStream.API_URL + 'refreshtoken?api-version=1.3-private')

    def get_streams(self, limit=100, skip=0):
        self.refresh_token()
        self.streams_list = []
        params = [
            ['$top', limit],
            ['$skip', skip],
            ['$orderby', 'publishedDate desc'],
            ['$expand', 'creator,events'],
            ['$filter', 'published and (state eq \'Completed\' or contentSource eq \'livestream\')'],
            ['api-version', '1.3-private'],
                ]

        resp = self.session.get(MsStream.API_URL + 'videos', params=params).json()["value"]

        self.streams_list.extend(resp)

        while len(resp) == limit:
            skip = skip + limit
            params[1][1] = skip
            resp = self.session.get(MsStream.API_URL + 'videos', params=params).json()["value"]
            self.streams_list.extend(resp)
        return self.streams_list

    def get_stream_by_id(self, uid):
        return self.session.get(MsStream.API_URL + 'videos/{}?api-version=1.3-private'.format(uid)).json()

    def get_video_by_id(self, uid):
        return self.get_video(self.get_stream_by_id(uid))
        
    def get_video(self, stream):
        video_id = stream["id"]
        name = stream["name"]
        video, audio = get_playlist(stream["playbackUrl"] + "(format=m3u8-aapl)")
        subs = get_subs(self.session, stream)
        enc_key = get_key(self.session, video)
        timestamp = stream["created"]
        return Video(video_id, name, video, audio, subs, enc_key, timestamp)

    def get_videos(self):
        if self.videos != None:
            return self.videos
        self.videos = []
        for stream in self.streams_list:
            self.videos.append(self.get_video(stream))
        return self.videos
class Video:
    def __init__(self, video_id, name, video, audio, subs, enc_key, timestamp):
        self.id = video_id
        self.name = name
        self.video = video
        self.video_full_uri = copy.deepcopy(video)
        self.audio = audio
        self.audio_full_uri = copy.deepcopy(audio)
        self.subs = subs
        self.enc_key = enc_key
        self.timestamp = timestamp
        self.change_key_uri()

    def add_base_uri(self):
        for segment in self.video_full_uri.segments:
            segment.uri = segment.base_uri + segment.uri
        for segment in self.audio_full_uri.segments:
            segment.uri = segment.base_uri + segment.uri

    def change_key_uri(self):
        video_keys = [self.video.keys[0]]
        audio_keys = [self.audio.keys[0]]
        audio_keys[0].uri = "file://tmp/{}/kek.key".format(self.id)
        video_keys[0].uri = "file://tmp/{}/kek.key".format(self.id)
        self.video.keys = audio_keys
        self.audio.keys = video_keys

    def dumps_files(self, path):
        self.add_base_uri()
        with open(get_path_file(path, "video_full.m3u8"), "w") as f:
            f.write(self.video_full_uri.dumps())
        with open(get_path_file(path, "audio_full.m3u8"), "w") as f:
            f.write(self.audio_full_uri.dumps())
        with open(get_path_file(path, "video_tmp.m3u8"), "w") as f:
            f.write(self.video.dumps())
        with open(get_path_file(path, "audio_tmp.m3u8"), "w") as f:
            f.write(self.audio.dumps())
        with open(get_path_file(path, "subtitles.vtt"), "w") as f:
            f.write(self.subs)

    def download(self, dest_path=os.getcwd(), filename=None):
        if filename == None:
            filename = self.id
        path = get_temp_folder(self.id)
        try:
            shutil.rmtree(path)
        except:
            pass

        print("Downloading...")
        os.mkdir(path)
        with open(get_path_file(path, "kek.key"), "wb") as f:
            f.write(b64decode(self.enc_key))
        self.dumps_files(path)
        # LOL shell injection
        os.system("aria2c -j 16 -x 16 -i {} -d {}".format(get_path_file(path, "video_full.m3u8"), path))
        os.system("aria2c -j 16 -x 16 -i {} -d {}".format(get_path_file(path, "audio_full.m3u8"), path))
        #convert subtitles
        os.system("ffmpeg -i {0}subtitles.vtt {0}subtitles.srt".format(path))

        if self.subs == "RIP":
            with open(get_path_file(path, "subtitles.srt"), "w") as f:
                f.write("""1
00:00:01,600 --> 00:00:10,200
No subtitles""")

        FFMPEG_CMD = ("ffmpeg -protocol_whitelist file,http,https,tcp,tls,crypto "
                      "-allowed_extensions ALL -i {0}audio_tmp.m3u8  -protocol_whitelist "
                      "file,http,https,tcp,tls,crypto -allowed_extensions ALL -i {0}video_tmp.m3u8 "
                      "-i {0}subtitles.srt "
                      "-async 1 -c copy -c:s mov_text -bsf:a aac_adtstoasc {1}.mp4").format(path, get_path_file(dest_path, filename))
        os.system(FFMPEG_CMD)

        shutil.rmtree(path)